# use_github_action_pkgcheck [plain]

    `file_name` must be a character argument

---

    `file_name` must be a single value

---

    `inputs` must be a named list!

# use_github_action_pkgcheck [ansi]

    [1m[22m[30m[47m`file_name`[49m[39m must be a character argument

---

    [1m[22m[30m[47m`file_name`[49m[39m must be a single value

---

    [1m[22m[30m[47m`inputs`[49m[39m must be a named list!

# use_github_action_pkgcheck [unicode]

    `file_name` must be a character argument

---

    `file_name` must be a single value

---

    `inputs` must be a named list!

# use_github_action_pkgcheck [fancy]

    [1m[22m[30m[47m`file_name`[49m[39m must be a character argument

---

    [1m[22m[30m[47m`file_name`[49m[39m must be a single value

---

    [1m[22m[30m[47m`inputs`[49m[39m must be a named list!

